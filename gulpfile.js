var gulp = require('gulp');
var minifyCSS = require('gulp-csso');
var minify = require('gulp-minify');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var zip = require('gulp-zip');
var forceDeploy = require('gulp-jsforce-deploy');
var ext_replace = require('gulp-ext-replace');
var rename = require("gulp-rename");
var runSequence = require('run-sequence');
var gulpSequence = require('gulp-sequence')
var clean = require('gulp-clean');
var env = require('gulp-env');

gulp.task('css', function(){
  return gulp.src('css/*.css')
    .pipe(minifyCSS())
    .pipe(gulp.dest('build/css'));
});

gulp.task('js', function() {
  return gulp.src(['js/config/*.js','js/controller/*.js','js/service/*.js','js/factory/*.js','js/directive/*.js','js/value/*.js'])
    .pipe(concat('all.js'))
	.pipe(uglify({mangle: false}))
    .pipe(gulp.dest('build/js/'));
});

gulp.task('app', function() {
  return gulp.src(['js/app.js'])
    .pipe(gulp.dest('build/js'));
});

gulp.task('bootstrap', function() {
  return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js'])
    .pipe(uglify({mangle: false}))
    .pipe(gulp.dest('build/lib/bootstrap')),
	 gulp.src(['node_modules/bootstrap/dist/css/bootstrap.min.css'])
    .pipe(minifyCSS())
    .pipe(gulp.dest('build/lib/bootstrap'));
});

gulp.task('jquery', function() {
  return gulp.src(['node_modules/jquery/dist/jquery.min.js'])
	.pipe(uglify())
  .pipe(gulp.dest('build/lib/jquery'));
});

gulp.task('views', function(){
  return gulp.src(['views/*.htm'])
    .pipe(gulp.dest('build/views'));
});

gulp.task('fields', function(){
  return gulp.src(['fields/*.htm'])
    .pipe(gulp.dest('build/fields'));
});

gulp.task('angular', function() {
  return gulp.src(['node_modules/angular/angular.js', 'node_modules/angular-route/angular-route.js'])
    .pipe(uglify())
    .pipe(gulp.dest('build/lib/angular'));
});

gulp.task('zipapp', function() {
   return gulp.src(['build/**'])
    .pipe(zip('AppAngular1.zip'))
    .pipe(gulp.dest('build'));
});

gulp.task('buildpackagexml', function() {
  return gulp.src(['apex/**'])
  .pipe(gulp.dest('package'));
});

gulp.task('buildstaticresource', function() {
    return gulp.src(['build/AppAngular1.zip'])
      .pipe(ext_replace('.resource'))
      .pipe(gulp.dest('package/staticresources'));
});

gulp.task('deploypackage', function() {
  
  return gulp.src('package/**', { base: "." })
    .pipe(zip('package.zip'))
    .pipe(forceDeploy({
       username: process.env.app_username, 
	     password: process.env.app_password,
		   loginUrl: process.env.app_loginurl,
       pollTimeout: 120*1000,
       pollInterval: 10*1000,
       version: '38.0'
    }));
});

gulp.task('cleanbuild', function () {
   return gulp.src(['build/*'], {read: false})
        .pipe(clean());
});

gulp.task('cleanpackage', function () {
   return gulp.src(['package/*'], {read: false})
        .pipe(clean());
});

gulp.task('build', gulpSequence('app','css','bootstrap','jquery','js','views','fields','angular','zipapp'));

gulp.task('clean', gulpSequence('cleanbuild','cleanpackage'));

gulp.task('buildpackage', gulpSequence('buildpackagexml','buildstaticresource'));

gulp.task('deploy', gulpSequence('clean','build','buildpackage','deploypackage'));

gulp.task('default', [ 'views', 'css','js','angular']);