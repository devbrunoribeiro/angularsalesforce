public with sharing class AngularForceAccountRepository {
    public AngularForceAccountRepository() { } 
    
    public static Account getAccount(Account account) {
        return [SELECT Id, Name, Phone,Type,NumberOfEmployees,CPF__c FROM Account WHERE Id = :account.Id limit 1];
    }
    
    public static List<Account> getAccounts() {
        return [SELECT Id, Name, Phone, Type, NumberOfEmployees FROM Account limit 20];
    }
    
    public static List<Account> getAccountSearch(String name) {
        return Database.query('SELECT Id, Name, Phone, Type, NumberOfEmployees FROM Account WHERE Name LIKE \'%' + name + '%\' limit 20');
    }

    public static Account addAccount(Account account) {
        insert account;
        return account;
    }

    public static void deleteAccount(Account account) {
        delete account;
    }

    public static Account updateAccount(Account account) {
        update account;
        return account;
    }
}