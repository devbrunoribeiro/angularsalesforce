public with sharing class AngularForceAccountService {
    public AngularForceAccountService() { } // empty constructor
    
    public static Account getAccount(Account account) {
        return AngularForceAccountRepository.getAccount(account);
    }
    
    public static List<Account> getAccounts() {
        return AngularForceAccountRepository.getAccounts();
    }
    
    public static List<Account> getAccountSearch(String name) {
        return AngularForceAccountRepository.getAccountSearch(name);
    }

    public static Account addAccount(Account account) {
       return AngularForceAccountRepository.addAccount(account);
    }

    public static Account updateAccount(Account account) {
       return AngularForceAccountRepository.updateAccount(account);
    }

    public static void deleteAccount(Account account) {
       AngularForceAccountRepository.deleteAccount(account);
    }
}