global with sharing class AngularForceController {
	public String accountName { get; set; }
    public static Account account { get; set; }
    public static List<Account> accounts { get; set; }
    public AngularForceController() { } // empty constructor
    
    @RemoteAction
    global static Account getAccount(Account account) {
        return AngularForceAccountService.getAccount(account);
    }
    
    @RemoteAction
    global static List<Account> getAccounts() {
        return AngularForceAccountService.getAccounts();
    }
    
    @RemoteAction
    global static List<Account> getAccountSearch(String name) {
         return AngularForceAccountService.getAccountSearch(name);
    }

    @RemoteAction
    global static Account addAccount(Account account) {
         return AngularForceAccountService.addAccount(account);
    }

    @RemoteAction
    global static void deleteAccount(Account account) {
         AngularForceAccountService.deleteAccount(account);
    }

    @RemoteAction
    global static Account updateAccount(Account account) {
        return AngularForceAccountService.updateAccount(account);
    }
}