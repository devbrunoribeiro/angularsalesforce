'use strict';
angular.module("myApp").value('AccountPickListValue', {
	type : [{ id: 'Prospect', name:'Prospect' },
			{ id: 'Customer - Direct', name:'Customer - Direct' },
			{ id: 'Customer - Channel', name:'Customer - Channel' },
			{ id: 'Channel Partner / Reseller', name:'Channel Partner / Reseller' },
			{ id: 'Installation Partner', name:'Installation Partner' },
			{ id: 'Technology Partner', name:'Technology Partner' },
			{ id: 'Other', name:'Other' }],
	other : [{ id: 'Prospect', name:'Prospect' },
			{ id: 'Customer - Direct', name:'Customer - Direct' },
			{ id: 'Customer - Channel', name:'Customer - Channel' },
			{ id: 'Channel Partner / Reseller', name:'Channel Partner / Reseller' },
			{ id: 'Installation Partner', name:'Installation Partner' },
			{ id: 'Technology Partner', name:'Technology Partner' },
			{ id: 'Other', name:'Other' }]
});