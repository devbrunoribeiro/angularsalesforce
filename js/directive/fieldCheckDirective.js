'use strict';
var path = document.getElementById('url').value;
angular.module("myApp").directive("fieldCheck",function(){
    console.log(path);
    return {
        templateUrl: path + "/fields/fieldText.htm",
        replace:true,
        restrict:"E",
        scope:{
            label:'@',
            valuefield:'=',
            disabled:'='
        }
    };
})