'use strict';
var path = document.getElementById('url').value;
angular.module("myApp").directive("fieldSelect",function(){
    console.log(path);
    return {
        templateUrl: path + "/fields/fieldSelect.htm",
        replace:true,
        restrict:"E",
        scope:{
            label:'@',
            picklist:'=',
            valuefield:'=',
            disabled:'='
        }
    };
})