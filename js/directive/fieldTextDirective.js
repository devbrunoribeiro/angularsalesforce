'use strict';
var path = document.getElementById('url').value;
angular.module("myApp").directive("fieldText",function(){
    console.log(path);
    return {
        templateUrl: path + "/fields/fieldText.htm",
        replace:true,
        restrict:"E",
        scope:{
            label:'@',
            valuefield:'=',
            disabled:'='
        }
    };
})