'use strict';
angular.module("myApp").controller('AccountDetailController', ['$scope','$location','$routeParams', 'AccountService','AccountFactory','AccountPickListValue','ShadowService', 
	function($scope,$location,$routeParams,AccountService,AccountFactory,AccountPickListValue,ShadowService) {
		
		var account = new AccountFactory();
		$scope.conta = account;

		$scope.edit = true;
		
		$scope.picklist = AccountPickListValue;

		var backSearch = function(){
			$location.path("/");
		};

		$scope.getAccount = function(){   
			var account = new AccountFactory($routeParams.id);
			AccountService.getAccount(account).then(successGetAccount,error);	
		};
		$scope.editAccount = function(){   
			$scope.edit = false;
		};
		$scope.deleteAccount = function(){   
			AccountService.deleteAccount($scope.conta).then(successDeleteAccount,error);	
		};
		$scope.updateAccount = function(){   
			console.log($scope.conta);
			AccountService.updateAccount($scope.conta).then(successUpdateAccount,error);	
		};

		var successGetAccount = function(result){
			$scope.conta = account.toObject(result);
		 };
		var successDeleteAccount = function(result){
			backSearch();
		};
		var successUpdateAccount = function(result){
			console.log('555555555581');
			backSearch();
		 };

		var error = function(error){
			$scope.conta = null;
		 };
}]);