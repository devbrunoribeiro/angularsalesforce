'use strict';
angular.module("myApp").controller('AccountSearchController', ['$scope', 'AccountService','AccountFactory','ShadowService', 
	function($scope,AccountService,AccountFactory,ShadowService) {
	
		$scope.contas = '';
		$scope.pNomeConta='';

		var toogleShadow = ShadowService.loadingToggle;
		
		$scope.GetAccounts = function(){   
			
			var account = new AccountFactory();
			AccountService.getAccounts().then(
				function(result) {$scope.contas = account.toList(result);},
				function(error)  {$scope.contas = null;}
			);	
		};
	
		$scope.GetAccountSearch = function(){
			
			var account = new AccountFactory();
			AccountService.searchAccount($scope.pNomeConta).then(
				function(result) {$scope.contas = account.toList(result);},
				function(error)  {$scope.contas = null;}
			);
		};
}]);