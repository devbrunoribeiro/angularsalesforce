'use strict';
angular.module("myApp").controller('AccountAddController', ['$scope','$location','$routeParams', 'AccountService','AccountFactory','AccountPickListValue', 
	function($scope,$location,$routeParams,AccountService,AccountFactory,AccountPickListValue) {
		
		//New object Account
		var account = new AccountFactory();
		$scope.conta = account;

		//is ever true, but is insert 
		$scope.edit = true;

		//Load value pickList fields
		$scope.picklist = AccountPickListValue;

		//Success Insert that Account
		var successAdd = function(result){
			$scope.conta = account.toObject(result);
			$location.path("/");
		};

		//Error Insert that Account
		var errorAdd = function(error){
			$scope.conta = null;
		};

		//Insert Account method
		$scope.addAccount = function(){   
			AccountService.addAccount($scope.conta).then(successAdd,errorAdd);	
		};
}]);