'use strict';
var path = document.getElementById('url').value;
angular.module("myApp",['ngRoute']).config(function($routeProvider) {
  $routeProvider
	.when("/", {
		templateUrl : path + '/views/account_search.htm',
		controller: 'AccountSearchController'
	})
	.when("/account/detail/:id", {
		templateUrl : path + '/views/account_detail.htm',
		controller: 'AccountDetailController'
	})
	.when("/account/add", {
		templateUrl : path + '/views/account_add.htm',
		controller: 'AccountAddController'
	})
});