'use strict';
angular.module("myApp").factory('AccountFactory', function() {

    var __toparse = function(json){
        var object = JSON.parse(json);
        return toObject(object);
    };

    var __toJsonString = function(){
        return JSON.stringify(this);
    };

    var __toList = function(objectList){
        var listobject=[];
        for(var i=0;i<objectList.length;i++){
           listobject.push(new __toObject(objectList[i])); 
        }
        return listobject;
    };

    var __toObject = function(object){
        this.Id = object.Id;
        this.Name = object.Name;
        this.Phone = object.Phone;
        this.Type = object.Type;
        this.CPF__c = object.CPF__c;
        return this;
    };

    var __toJSON = function() {
        var copy = this; 
        return copy; 
    };

    var AccountFactory = function(Id,Name,Phone,Type,CPF__c) {
        this.Id = Id;
        this.Name = Name;
        this.Phone = Phone;
        this.Type = Type;
        this.CPF__c = CPF__c;
        this.parse = __toparse;
        this.toJSON = __toJSON;
        this.toObject = __toObject;
        this.toJsonString = __toJsonString;
        this.toList = __toList;
        return this;
    };

	return AccountFactory;

});