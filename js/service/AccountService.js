'use strict';
angular.module("myApp").service('AccountService', ['VisualRemoteService', function(VisualRemoteService) {
	
	this.getAccounts = function(){
		var action = 'AngularForceController.getAccounts';
		return VisualRemoteService(action,null);
	};
	
	this.getAccount = function(request){
		var action = 'AngularForceController.getAccount';
		return VisualRemoteService(action,request);
	};
	
	this.searchAccount = function(request){
		var action = 'AngularForceController.getAccountSearch';
		return VisualRemoteService(action,request);
	};

	this.addAccount = function(request){
		var action = 'AngularForceController.addAccount';
		return VisualRemoteService(action,request);
	};

	this.updateAccount = function(request){
		var action = 'AngularForceController.updateAccount';
		return VisualRemoteService(action,request);
	};

	this.deleteAccount = function(request){
		var action = 'AngularForceController.deleteAccount';
		return VisualRemoteService(action,request);
	};

}]);