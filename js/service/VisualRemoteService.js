'use strict';
angular.module("myApp").factory('VisualRemoteService', ['$q', '$rootScope', function($q, $rootScope) {
	
	var config = { buffer: true, escape: true, timeout: 30000 };
	
	var deferred;
	
	var callback = function(result, event) {
		$rootScope.$apply(function() {
		  if (event.status) {
			deferred.resolve(result);
		  } else {
			deferred.reject(event);
		  }
		})
	};
	
	var VisualRemote = function(action,request){
		deferred = $q.defer(); 
		if(request!=null){
		   Visualforce.remoting.Manager.invokeAction(action,request,callback,config);
		}else{
		   Visualforce.remoting.Manager.invokeAction(action,callback,config);
		}
		return deferred.promise;
	};
	
	return VisualRemote;

}]);