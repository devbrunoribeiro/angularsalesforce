# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is sample application Salesforce, built in :

* Angular.JS
* Salesforce Apex
* Gulp

### What is this repository for? ###

	.  
	├── gulpfile.js  
	├── apex  
	|   ├── classes  
	|   └── pages  
	|   └── staticresources  
	|   └── package.xml  
	├── css  
	|   └── style.css  
	├── fields  
	|   ├── fieldCheck.htm  
	|   └── fieldSelect.htm  
	|   └── fieldText.htm  
	├── js  
	|   ├── app.js  
	|   └── config  
	|   └── controller  
	|   └── directive  
	|   └── factory  
	|   └── service  
	|   └── value  
	├── views  
	├── node_modules  
	├── package /* temporally folder */  
	├── build /* temporally folder */  
	└── index.html  

### How do I get set up? ###
* Pull repository, in your workspace.
* Download NodeJs and installed in your machine. url:https://nodejs.org/en/
* Run command npm install in directory of project. 
* Set variables for enviroment
	- app_loginurl - this is url in your org 
		- production is https://login.salesforce.com
		- sandbox is https://test.salesforce.com
	- app_password - this is password in your org, more "security token".
	- app_username - this is user in your org.
* tasks of application for gulp more importants 
	- gulp build
	- gulp deploy 
* run visualforce application in your org
### Contribution guidelines ###

* Bruno Smith Lopes Ribeiro

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact